﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameOverPanel : MonoBehaviour
{
    public Text score;
    public GameObject newRecord;

    public Text previusRecord;

    public Image button;
    float buttonTimer;
    private void OnEnable()
    {
        int newScore = (int)GameManager.instance.totalTime;
        score.text = "Time survived: " + newScore.ToString() + "s";

        int record = PlayerPrefs.GetInt("HighScore", 0);

        previusRecord.text = "Longest survived: " + record.ToString() + "s";

        if (record < newScore)
        {
            PlayerPrefs.SetInt("HighScore", newScore);
            newRecord.SetActive(true);
        }

       
    }

    private void Update()
    {
        if (Input.GetButton("Fire1"))
        {
            buttonTimer += Time.deltaTime / 2f;
            button.fillAmount = buttonTimer;

            if (buttonTimer >= 1f)
                ReloadLvl();
        }

        else if (Input.GetButtonUp("Fire1"))
        {
            buttonTimer = 0f;
            button.fillAmount = buttonTimer;
        }
    }

    void ReloadLvl()
    {
        SceneManager.LoadScene("EndlessShooter");
    }
}
