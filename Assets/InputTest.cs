﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Gvr.Internal;

public class InputTest : MonoBehaviour
{
    public Text testText;

    
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetButton("Fire1"))
        {
            testText.color = new Color(1, 0, 0);
        }

        else testText.color = new Color(0, 1, 0);
    }
}
