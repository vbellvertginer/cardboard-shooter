﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HeatUI : MonoBehaviour
{
    public Weapon weapon;

    public Image heatLvlImage;
    public Image overHeatLvlImage;
    float maxHeat;

    // Start is called before the first frame update
    void Start()
    {
        maxHeat = weapon.heatLimit;

    }

    // Update is called once per frame
    void Update()
    {
        
        if (!weapon.overHeat)
        {
            if (!heatLvlImage.gameObject.activeInHierarchy)
            {
                heatLvlImage.gameObject.SetActive(true);
                overHeatLvlImage.gameObject.SetActive(false);
            }
            heatLvlImage.fillAmount = weapon.heat / maxHeat;
        }
        else
        {
            if (!overHeatLvlImage.gameObject.activeInHierarchy)
            {
                heatLvlImage.gameObject.SetActive(false);
                overHeatLvlImage.gameObject.SetActive(true);
            }
            overHeatLvlImage.fillAmount = weapon.heat / maxHeat;
        }


    }
}
