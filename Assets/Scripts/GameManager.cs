﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    /*Score sistem based in total time survived, timer in seconds decreases, if reches 0 gameover,
    killing enemies increse timer, enemies reaching you decreases it,
    killing green cubes decreaeses timer and if they reach you it increases it
    */
    public int gameTimer;

    public float totalTime;
    public float startTime;

    public Text timerText;
    float previusTimeText;

    public bool gameOver = false;

    public GameObject gameOverPanel;
    #region Singleton
    public static GameManager instance;

    void Awake()
    {
        instance = this;
    }
    #endregion

    private void Start()
    {
        startTime = Time.time;
        StartCoroutine(GameTimer());
    }
    
    //Coroutine with 1 second delay to be used as a timer for the game score system
    IEnumerator GameTimer()
    {
        while (!gameOver)
        {
            yield return new WaitForSeconds(1f);
            gameTimer--;

            UpdateText();

            if (gameTimer <= 0)
            {
                GameOver();
            }
        }
    }

    void UpdateText()
    {
        timerText.text = gameTimer.ToString();
    }

    public void ModifyTimer (int amount)
    {
        gameTimer += amount;
        UpdateText();


    }

    public void GameOver()
    {
        gameOver = true;
        totalTime = Time.time - startTime;
        gameOverPanel.SetActive(true);
    }
}
