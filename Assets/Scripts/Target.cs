﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Target : MonoBehaviour
{

    public int health;
    public float speed;
    public Vector3 movementRandomness;

    public int timeBonus = 5;

    Vector3 destination;
    int initialHeath;

    
    
    //Set default health to the public variables in order to reset the enemy when is reenabled
    private void Awake()
    {
        initialHeath = health;
        destination = GameObject.FindGameObjectWithTag("Player").transform.position;
    }

    private void OnEnable()
    {
        health = initialHeath;
    }

    protected virtual void Update()
    {
        if (!GameManager.instance.gameOver)
        { 
            Vector3 newPosition = Vector3.MoveTowards(transform.position, destination, speed * Time.deltaTime);

            newPosition += Vector3.Cross(new Vector3(Random.Range(-1f, 1f), Random.Range(-1f, 1f), Random.Range(-1f, 1f)),movementRandomness);
            transform.position = newPosition;

            if (Vector3.Distance(transform.position, destination) < 0.3f)
                DoDamage();
        }
    }

    public virtual void DoDamage()
    {
        GameManager.instance.ModifyTimer(timeBonus * -2);
        gameObject.SetActive(false);
    }

    public virtual void TakeDamage(int damage)
    {
        health -= damage;

        if (health <= 0)
        {
            Death();
        }
    }

    public virtual void Death()
    {
        GameManager.instance.ModifyTimer(timeBonus);
        gameObject.SetActive(false);
    }
}
