﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawner : MonoBehaviour
{

    public float maxDistance;
    public float minDistance;

    public float minHeight;
    public float maxHeight;


    //Use same order to reference all the enemies in the following Lists and arrays, this simplifies the code to randomly pick a new enemy and spawn it;
    public List<List<GameObject>> enemyPools = new List<List<GameObject>>();
    public int[] enemyPoolSize;
    public GameObject[] enemyPrefabs;
    public Transform[] enemyContainers;

    float spawnTimer;

    public float spawnRate;

    //Singleton to acces it wihtout references
    public static EnemySpawner instance;

    //Populate the list of instantiated prefabs with the amount specified in the public array
    private void Awake()
    {
        instance = this;
        for (int i = 0; i < enemyPrefabs.Length; i++)
        {
            enemyPools.Add(new List<GameObject>());
            for (int a = 0; a < enemyPoolSize[i]; a++)
            {
                enemyPools[i].Add(Instantiate(enemyPrefabs[i], enemyContainers[i]));
            }
        }
    }
   

    //Timer function to spawn enemies
    void Update()
    {
        //SpawnNewEnemy();
        if (!GameManager.instance.gameOver)
        {
            if (spawnTimer > spawnRate)
            {
                SpawnNewRandomEnemy();
                spawnTimer = 0f;
            }
            else
            {
                spawnTimer += Time.deltaTime;
            }
        }

    }

    void SpawnNewRandomEnemy()
    {
        //Debug.Log("spawn new enemy");
        //Use the time to increase difficulty by increasing number of new enemies every 30 seconds
        float difficulty = (Time.time - GameManager.instance.startTime) / 30f;

        //Spawn a new eneymy based on the difficulty
        for (int i = 0; i <= (int)difficulty; i++)
        {
            //Debug.Log("spawn new enemy");
            int listToUse = Random.Range(0, enemyPools.Count);

            GameObject newEnemy = NextInactiveInList(listToUse);

            newEnemy.transform.position = RandomNewPosition();

            newEnemy.SetActive(true);
        }
        
    }

    //Method to specify the list and amount of enemies to spawn
    public void SpawnNewEnemy(int type, int amount, Transform trans)
    {
        for (int i = 0; i < amount; i++)
        {
            GameObject newEnemy = NextInactiveInList(type);

            newEnemy.transform.position = trans.position + new Vector3(Random.Range(-1f,1f), Random.Range(-0.5f, 0.5f), Random.Range(-1f, 1f));

            newEnemy.SetActive(true);
        }

    }

    GameObject NextInactiveInList(int list)
    {
        for (int i = 0; i < enemyPools[list].Count; i++)
        {
            if (!enemyPools[list][i].activeInHierarchy)
                return enemyPools[list][i];
        }

        //if theres not inactive enemy in the list instantiate a new one and add it to the list
        GameObject newInstance = Instantiate(enemyPrefabs[list], enemyContainers[list]);
        enemyPools[list].Add(newInstance);

        return newInstance;

    }

    //Generate a new random position around the player to spawn the enemies
    Vector3 RandomNewPosition()
    {
        Vector2 radiusPoint = Random.insideUnitCircle.normalized * Random.Range(minDistance,maxDistance);

        return new Vector3(radiusPoint.x, Random.Range(minHeight,maxHeight), radiusPoint.y);
    }

    //Draw gizmos on the scene when the spawner is selected to show the min and max distance on which the enemies will be spawned
#if UNITY_EDITOR
    void OnDrawGizmosSelected()
    {
            UnityEditor.Handles.color = Color.green;
            UnityEditor.Handles.DrawWireDisc(transform.position, transform.up, minDistance);
            UnityEditor.Handles.color = Color.red;
            UnityEditor.Handles.DrawWireDisc(transform.position, transform.up, maxDistance);
    }
#endif
}
