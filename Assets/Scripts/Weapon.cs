﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Weapon : MonoBehaviour
{
    bool shooting;
    float shootingTimer;

    //Damage
    public float fireRate;
    public int damage = 10;
    //Heat system variables
    public float heat;
    public float heatLimit = 10f;
    public float coolingRate = 3;
    public bool overHeat;

    public Text debugText;

    public GameObject bulletPrefab;
    List<GameObject> bullets = new List<GameObject>();
    public Transform weaponMuzzle;

    public Transform bulletContainer;

    // Start is called before the first frame update
    void Start()
    {
        for (int i = 0; i < 20; i++)
        {
            bullets.Add(Instantiate(bulletPrefab, bulletContainer));
        }

        //Start the timer with the value to trigger a shot immediatly after pressing the button
        shootingTimer = fireRate;
    }

    // Update is called once per frame
    void Update()
    {
        //
        if (!GameManager.instance.gameOver)
        {  
            if (Input.GetButtonDown("Fire1") && !shooting)
            {
                shooting = true;
            }

            else if (Input.GetButtonUp("Fire1") && shooting)
            {
                shooting = false;
                shootingTimer = fireRate; //Make sure the first shoot hapens immediatly after pressing the button
            }

            if (shooting && !overHeat)
            {
                shootingTimer += Time.deltaTime;

                heat += Time.deltaTime;

                if (heat > heatLimit) overHeat = true;

                if (shootingTimer >= fireRate)
                { 
                    Shoot();
                    shootingTimer = 0f;
                }
            }

            else if (heat > 0f)
            {
           
                heat -= Time.deltaTime * coolingRate;

                if (overHeat && heat <= 0f)
                {
                    overHeat = false;
                }
            }
        }

    }

    void Shoot()
    {
        GameObject tempBullet = NextInactiveInList();

        tempBullet.transform.position = weaponMuzzle.transform.position;
        tempBullet.transform.rotation = weaponMuzzle.transform.rotation;
        tempBullet.SetActive(true);
    }


    GameObject NextInactiveInList()
    {
        for (int i = 0; i < bullets.Count; i++)
        {
            if (!bullets[i].activeInHierarchy)
                return bullets[i];
        }

        //if theres not inactive enemy in the list instantiate a new one and add it to the list
        GameObject newInstance = Instantiate(bulletPrefab, bulletContainer);
        bullets.Add(newInstance);

        return newInstance;

    }
}
