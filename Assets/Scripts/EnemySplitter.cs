﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySplitter : Target
{
    public override void Death()
    {
        base.Death();

        EnemySpawner.instance.SpawnNewEnemy(0, 2, transform);
        //Debug.Log("Now you have 2 more enemies!");
    }
 
}
