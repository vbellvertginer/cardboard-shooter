# Cardboard shooter

In order to test the game locally:

1.- Download the APK from https://bitbucket.org/vbellvertginer/unit9-test/downloads/EndelessShooter_v1.0.apk

2.- Copy the APK file to a known location in an Android phone, in case you performed step 1 directly on an Android phone jump into step three.

3.- Using the file explorer in the device search the APK and install it.

4.- Start the game and enjoy!
